//adb -s 55fuhugut879xkbu  reverse tcp:8081 tcp:8081
//npx react-native run-android

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import Geolocation from '@react-native-community/geolocation';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Platform,
  AppRegistry,
  AppState,
  ScrollView,
} from 'react-native';
import {TextInput, Button} from 'react-native-paper';
import MapView, {Marker, Polyline, PROVIDER_GOOGLE} from 'react-native-maps';
import haversine from 'haversine';
//import Background from 'Background';

/*const Background = async data => {
  console.log('sdf');
  navigator.geolocation.getCurrentPosition(position => {
    console.log(position.coords);
  });
};
AppRegistry.registerHeadlessTask('Background', () => Background);*/

const LATITUDE_DELTA = 0.009;
const LONGITUDE_DELTA = 0.009;
const LATITUDE = 0;
const LONGITUDE = 0;
let markers;
let isLogged = false;
Date.prototype.addHours = function (h) {
  this.setHours(this.getHours() + h);
  return this;
};
class AnimatedMarkers extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      id: null,
      device: '',
      password: '',
      isLogged: false,
      latitude: LATITUDE,
      longitude: LONGITUDE,
      routeCoordinates: [],
      distanceTravelled: 3,
      prevLatLng: {},
      coordinate: {
        latitude: LATITUDE,
        longitude: LONGITUDE,
        latitudeDelta: 0,
        longitudeDelta: 0,
      },
    };

    markers = [
      {
        title: 'hello',
        coordinates: {
          latitude: 0,
          longitude: 0,
        },
      },
    ];

    Geolocation.getCurrentPosition(
      position => {
        const location = JSON.stringify(position);
        const {latitude, longitude} = position.coords;
        console.log(
          'INit Loc: ' +
            position.coords.latitude +
            ' : ' +
            position.coords.longitude,
        );
        this.setState({
          latitude: latitude,
          longitude: longitude,
          prevLatLng: {latitude, longitude},
        });
      },
      error => console.log('error_Find: ' + error.message),
      {enableHighAccuracy: false, timeout: 20000, maximumAge: 1000},
    );
  }

  state1 = {
    appState: AppState.currentState,
  };

  findCoordinates() {
    if (this.state.isLogged) {
      const {coordinate} = this.state;
      Geolocation.getCurrentPosition(
        position => {
          const {routeCoordinates, distanceTravelled} = this.state;
          const _latitude = position.coords.latitude;
          const _longitude = position.coords.longitude;
          console.log(
            'Login Loc: ' +
              position.coords.latitude +
              ' : ' +
              position.coords.longitude,
          );
          const newCoordinate = {latitude: _latitude, longitude: _longitude};
          console.log(newCoordinate);
          if (Platform.OS === 'android') {
          } else {
            coordinate.timing(newCoordinate).start();
          }

          if (this.state.isLogged) {
            /*          alert(
                        'Sended pos: ' +
                          position.coords.latitude +
                          ' : ' +
                          position.coords.longitude,
                      );*/
            this.setState({
              _latitude,
              _longitude,
              routeCoordinates: routeCoordinates.concat([newCoordinate]),
              distanceTravelled:
                distanceTravelled + this.calcDistance(newCoordinate),
              prevLatLng: newCoordinate,
            });
            this.sendPosition(
              position.coords.latitude,
              position.coords.longitude,
            );
          }
        },
        error => console.log(error),
        {
          enableHighAccuracy: false,
          timeout: 20000,
          maximumAge: 1000,
          distanceFilter: 10,
        },
      );
    }
  }

  componentDidMount() {
    AppState.addEventListener('change', this._handleAppStateChange);

    const {coordinate} = this.state;

    this.watchID = Geolocation.watchPosition(
      position => {
        const {routeCoordinates, distanceTravelled} = this.state;
        const _latitude = position.coords.latitude;
        const _longitude = position.coords.longitude;
        const newCoordinate = {latitude: _latitude, longitude: _longitude};

        if (Platform.OS === 'android') {
        } else {
          coordinate.timing(newCoordinate).start();
        }

        if (this.state.isLogged) {
          /*          alert(
            'Sended pos: ' +
              position.coords.latitude +
              ' : ' +
              position.coords.longitude,
          );*/
          this.setState({
            _latitude,
            _longitude,
            routeCoordinates: routeCoordinates.concat([newCoordinate]),
            distanceTravelled:
              distanceTravelled + this.calcDistance(newCoordinate),
            prevLatLng: newCoordinate,
          });
          this.sendPosition(
            position.coords.latitude,
            position.coords.longitude,
          );
        }
      },
      error => console.log(error),
      {
        enableHighAccuracy: false,
        timeout: 20000,
        maximumAge: 1000,
        distanceFilter: 10,
      },
    );
  }

  sendPosition(lat, long) {
    let date = new Date()
      .addHours(2)
      .toISOString()
      .slice(0, 19)
      .replace('T', ' ');
    console.log(
      'https://sysmobproj.000webhostapp.com/api/add_position.php?id=' +
        this.state.id +
        '&date=' +
        date +
        '&lat=' +
        lat +
        '&lng=' +
        long,
    );

    fetch(
      'https://sysmobproj.000webhostapp.com/api/add_position.php?id=' +
        this.state.id +
        '&date=' +
        date +
        '&lat=' +
        lat +
        '&lng=' +
        long,
    );
  }

  componentWillUnmount() {
    AppState.removeEventListener('change', this._handleAppStateChange);
    Geolocation.clearWatch(this.watchID);
  }

  _handleAppStateChange = nextAppState => {
    console.log(this.state1.appState);
    if (
      this.state1.appState.match(/inactive|background/) &&
      nextAppState === 'active'
    ) {
      console.log('App has come to the foreground!');
    }
    this.setState({appState: nextAppState});
  };

  getMapRegion = () => ({
    latitude: this.state.latitude,
    longitude: this.state.longitude,
    latitudeDelta: LATITUDE_DELTA,
    longitudeDelta: LONGITUDE_DELTA,
  });

  calcDistance = newLatLng => {
    const {prevLatLng} = this.state;
    return haversine(prevLatLng, newLatLng) || 0;
  };

  login() {
    fetch(
      'https://sysmobproj.000webhostapp.com/api/login.php?device=' +
        this.state.device +
        '&password=' +
        this.state.password,
    )
      .then(res => res.json())
      .then(
        result => {
          this.setState({
            id: result[0].id,
            pass: result[0].password,
            device: result[0].device,
            isLogged: true,
          });
          console.log(this.state.id);
          this.findCoordinates();
        },
        // Uwaga: to ważne, żeby obsłużyć błędy tutaj, a
        // nie w bloku catch(), aby nie przetwarzać błędów
        // mających swoje źródło w komponencie.
        error => {
          alert('Błędne dane!');
          console.log('err: ' + error);
        },
      );
  }

  register() {
    if (this.state.device.length < 1) {
      alert('Za krórtka nazwa urządzenia!');
    } else if (this.state.password.length < 5) {
      alert('Hasło powinno zawierać minimum 4 znaki!');
    } else {
      fetch(
        'https://sysmobproj.000webhostapp.com/api/register.php?device=' +
          this.state.device +
          '&password=' +
          this.state.password,
      )
        .then(res => res.json())
        .then(
          result => {
            this.setState({
              id: result[0].id,
              pass: result[0].password,
              device: result[0].device,
              isLogged: true,
            });
            console.log(this.state.id);
          },
          // Uwaga: to ważne, żeby obsłużyć błędy tutaj, a
          // nie w bloku catch(), aby nie przetwarzać błędów
          // mających swoje źródło w komponencie.
          error => {
            alert('Błędne dane!');
            console.log('err: ' + error);
          },
        );
    }
  }

  render() {
    console.log(this.state.routeCoordinates);
    /*    return (
      <NavigationContainer>
        <Stack.Navigator
          initialRouteName="LoginRegister"
          screenOptions={{
            headerShown: false,
          }}>
          <Stack.Screen name="LoginRegister" component={LoginRegister} />
        </Stack.Navigator>
      </NavigationContainer>
    );*/

    if (this.state.isLogged) {
      return (
        <View style={styles.container}>
          <MapView
            style={styles.map}
            provider={PROVIDER_GOOGLE}
            annotations={this.markers}
            showUserLocation
            followUserLocation
            loadingEnabled
            region={this.getMapRegion()}>
            {this.state.routeCoordinates.length > 0 ? (
              <Polyline
                coordinates={this.state.routeCoordinates}
                strokeWidth={5}
              />
            ) : null}
          </MapView>
          <View style={styles.buttonContainer}>
            <TouchableOpacity style={[styles.bubble, styles.button]}>
              <Text>
                {parseFloat(this.state.distanceTravelled).toFixed(2)} km
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      );
    } else {
      return (
        <ScrollView contentContainerStyle={styles.container}>
          <TextInput
            style={styles.input}
            value={this.state.device}
            onChangeText={p => this.setState({device: p})}
            placeholder="Urządzenie"
            keyboardType="email-address"
          />

          <TextInput
            style={styles.input}
            value={this.state.password}
            onChangeText={p => this.setState({password: p})}
            placeholder="Hasło"
            secureTextEntry={true}
          />

          <Button
            style={styles.button}
            title="Logowanie"
            mode="contained"
            onPress={() => {
              this.login();
            }}>
            Logowanie
          </Button>
          <Button
            style={styles.button}
            title="Rejestracja"
            mode="contained"
            onPress={() => this.register()}>
            Rejestracja
          </Button>
        </ScrollView>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  bubble: {
    flex: 1,
    backgroundColor: 'rgba(255,255,255,0.7)',
    paddingHorizontal: 18,
    paddingVertical: 12,
    borderRadius: 20,
  },
  latlng: {
    width: 200,
    alignItems: 'stretch',
  },
  button: {
    padding: 15,
    textAlign: 'center',
    marginHorizontal: 10,
    marginVertical: 10,
  },
  input: {
    backgroundColor: 'rgba(0,0,0,0.14)',
    paddingVertical: 12,
  },
  buttonContainer: {
    position: 'absolute',
    bottom: 20,
    flexDirection: 'row',
    marginVertical: 20,
    backgroundColor: 'transparent',
  },
});

export default AnimatedMarkers;
